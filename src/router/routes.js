
const routes = [
  { path: '', component: () => import('pages/CRUD.vue') },
  { path: '/login', component: () => import('pages/Login.vue') },
  { path: '/create_account', component: () => import('pages/CreateAccount.vue') },
  { path: '/profile', component: () => import('pages/Profile.vue') },
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
